import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const server = {

    baseURL: 'https://84840900.eu.ngrok.io'

}

export const store = new Vuex.Store({
    state: {
        datos: [],
        usuario: []
    },
    getters: {
        session: state => state.datos,
        usuario: state => state.usuario
    }
});