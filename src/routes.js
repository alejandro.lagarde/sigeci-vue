import Login from './components/public/Login.vue'
import Home from './components/Home.vue'
import Docentes from './components/director/Docentes.vue'
// import Carreras from './components/admin/Carreras.vue'
// import Divisiones from './components/admin/Divisiones.vue'
import PaseLista from './components/docente/PaseLista.vue'
import SubirCalificaciones from './components/docente/SubirCalificaciones.vue'
import HistorialAcademico from './components/estudiante/HistorialAcademico.vue'
import EvaluacionDocente from './components/estudiante/EvaluacionDocente.vue'
import PreguntasEvaluacion from './components/escolares/PreguntasEvaluacion.vue'
import AsignacionGrupos from './components/escolares/AsignacionGrupos.vue'
import AsignacionAspirantes from './components/escolares/AsignacionAspirantes.vue'
import AsignacionMaterias from './components/director/AsignacionMaterias.vue'
import Directores from './components/admin/Directores.vue'
// import Grupos from './components/escolares/Grupos.vue'
import DivisionesCarreras from './components/admin/DivisionesCarreras.vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'sweetalert2/dist/sweetalert2.min.css';
import NotFound from './components/NotFound.vue'
import NotAutorized from './components/NotAutorized.vue'

// import { store } from './store'



export const routes = [
    { path: '/login', name: 'login', component: Login },
    {

        path: '/home',
        name: 'home',
        component: Home
    },

    //------------------- VISTAS DOCENTE-------------------
    {

        path: '/PaseLista',
        name: 'paseLista',
        component: PaseLista
    },
    {
        path: '/DivisionesCarreras',
        name: 'divisionesCarreras',
        component: DivisionesCarreras
    },
    {
        path: '/subirCalificaciones',
        name: 'subirCalificaciones',
        component: SubirCalificaciones
    },
    {
        path: '/asignacionGrupos',
        name: 'asignacionGrupos',
        component: AsignacionGrupos
    },
    {
        path: '/Docentes',
        name: 'docentes',
        component: Docentes
    },
    {
        path: '/HistorialAcademico',
        name: 'HistorialAcademico',
        component: HistorialAcademico
    },
    {
        path: '/EvaluacionDocente',
        name: 'EvaluacionDocente',
        component: EvaluacionDocente
    },
    {
        path: '/Directores',
        name: 'Directores',
        component: Directores
    },
    {
        path: '/AsignacionGrupos',
        name: 'AsignacionGrupos',
        component: AsignacionGrupos
    },
    {
        path: '/AsignacionMaterias',
        name: 'AsignacionMaterias',
        component: AsignacionMaterias
    },
    {
        path: '/AsignacionAspirantes',
        name: 'AsignacionAspirantes',
        component: AsignacionAspirantes
    },
    {
        path: '/PreguntasEvaluacion',
        name: 'PreguntasEvaluacion',
        component: PreguntasEvaluacion
    },
    { path: '/NotAutorized', component: NotAutorized },
    { path: '*', component: NotFound },



]