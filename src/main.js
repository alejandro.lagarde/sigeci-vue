import Vue from 'vue'
import App from './App.vue'
import firebase from 'firebase'
import VueRouter from 'vue-router'
import { routes } from './routes'
import { store } from './store'
import 'bootstrap'
// import 'bootstrap/dist/css/bootstrap.min.css'
import 'material-icons/iconfont/material-icons.css'
import VueSweetalert2 from 'vue-sweetalert2';
import BootstrapVue from 'bootstrap-vue'
import Vuelidate from 'vuelidate'

Vue.use(Vuelidate)
Vue.use(BootstrapVue)
Vue.use(VueRouter);
Vue.use(VueSweetalert2);


firebase.initializeApp({
    apiKey: "AIzaSyCEWswIAcVid52mtQwc31FLYaikV0ygy90",
    authDomain: "sigeci-vue.firebaseapp.com",
    databaseURL: "https://sigeci-vue.firebaseio.com",
    projectId: "sigeci-vue",
    storageBucket: "sigeci-vue.appspot.com",
    messagingSenderId: "421485663694",
    appId: "1:421485663694:web:68195c9f44fa90923b479f"
});
const router = new VueRouter({
    mode: 'history',
    routes
});


// router.beforeEach((to, from, next) => {
//     if (store.state.user.auth) {
//         next();
//     } else {
//         next({ path: '/login' });
//     }
//     next();
// });



Vue.config.productionTip = false

new Vue({
    render: h => h(App),
    router,
    store
}).$mount('#app')