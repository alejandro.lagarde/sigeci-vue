export const functions = {
    methods: {
        showModal(ref) {
            this.$refs[ref].show()
        },
        hideModal(ref) {
            this.$refs[ref].hide()
        },
        alertSuccess(message) {
            this.$swal({
                position: 'top-end',
                // icon: 'success',
                title: message,
                showConfirmButton: false,
                timer: 2000
            });
        },
        alertError(message) {
            this.$swal({
                position: 'top-end',
                // icon: 'error',
                title: message,
                showConfirmButton: false,
                timer: 2000
            });
        },
        confirmacion(){
           this.$swal.fire({
                title: 'Alerta',
                text: "¿Decea confirmar la acción?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si'
              }).then((result) => {
                if (result.value) {
                  return true;
                }
                return false;
              })
        }
    }
}